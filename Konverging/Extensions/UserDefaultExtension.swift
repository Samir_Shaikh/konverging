import UIKit
import SwiftyUserDefaults

extension UserDefaults {
    
    /* UserModel Default Keys */
    subscript(key: DefaultsKey<UserInfoModel?>) -> UserInfoModel? {
        get { return unarchive(key) }
        set { archive(key, newValue) }
    }
  
    static func removeAllData() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
}

// MARK: - Default keys
extension DefaultsKeys {
    
    static let isUserLoggedIn      = DefaultsKey<Bool>(StringConstants.UserDefaultKey.isUserLoggedIn)
    
    // User models
    static let userInfo            = DefaultsKey<UserInfoModel?>(StringConstants.UserDefaultKey.userInfo)
    
}

