import Foundation
import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD

struct keyboardManager {
    
    static func setEnabled(enable: Bool = true) {
        
        IQKeyboardManager.shared.enable = enable
    }
}

struct loader {
    
    static func show() {
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
    }
    
    static func dismiss() {
        
        SVProgressHUD.dismiss()
    }
}

//MARK: - Navigation Bar Item Options
struct NavigationBarOptions {
    
    let leftBtnImage: UIImage?
    let rightBtnImage: UIImage?
    let extraLeftBtnImage: UIImage?
    let extraRightBtnImage: UIImage?
    
    init(leftBtnImage: UIImage? = #imageLiteral(resourceName: "Menu"), rightBtnImage: UIImage? = nil, extraLeftBtnImage: UIImage? = nil, extraRightBtnImage: UIImage? = nil) {
        
        self.leftBtnImage = leftBtnImage
        self.rightBtnImage = rightBtnImage
        self.extraLeftBtnImage = extraLeftBtnImage
        self.extraRightBtnImage = extraRightBtnImage
    }
}

extension UIView {
    
    //MARK: - Round Corners
    func roundCorners(cornerRadius: CGFloat = 5, borderColor: UIColor = UIColor.clear, borderWidth: CGFloat = 1) {
        
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }
}

extension UIViewController {
    
    typealias keyboardReturnKeyHandler = IQKeyboardReturnKeyHandler
    
    func initializeReturnKeyHandler() -> keyboardReturnKeyHandler {
        
        let returnKeyHandler = IQKeyboardReturnKeyHandler.init(controller: self)
        returnKeyHandler.lastTextFieldReturnKeyType = .done
        return returnKeyHandler
    }
    
    func showAlert(title: String? = nil, message: String?, didDismiss: (() -> Void)? = nil) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
            
            didDismiss?()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func showPicker(withData data: [String], andTitle title: String, didSelectRow: @escaping ((Int) -> Void), didTapBackground: (() -> Void)? = nil) {
        
        var picker = Bundle.main.loadNibNamed("PickerView", owner: nil, options: nil)?[0] as? PickerView
        
        picker?.frame = UIScreen.main.bounds
        picker?.setData(title: title, data: data)
        picker?.doneCallback = { (selectedRow) in
            
            didSelectRow(selectedRow)
            picker?.removeFromSuperview()
            picker = nil
        }
        picker?.backgroundTapCallback = {
            
            didTapBackground?()
            picker?.removeFromSuperview()
            picker = nil
        }
        if let picker = picker {
            
            view.addSubview(picker)
        }
    }
    
    //MARK: - Configure Background Tap to End Editing
    func setUpBackgroundTap() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func didTapView(_ sender: Any) {
        
        view.endEditing(true)
    }
    
    func showCountryPicker(didSelectCountry: @escaping (String, String) -> Void) {
        
        var countries = [String]()
        var countryCodes = [String]()
        for countryCode: String in NSLocale.isoCountryCodes {
            
            let country = NSLocale.system.localizedString(forRegionCode: countryCode)
            
            if let country = country {
                
                countries.append(country)
                countryCodes.append(countryCode)
            }
        }
        
        showPicker(withData: countries, andTitle: "Select Country", didSelectRow: { selectedRow in
            
            didSelectCountry(countries[selectedRow], countryCodes[selectedRow])
        })
    }
    
    //MARK: - Set Up Navigation Bar
    func setUpNavigationBar(isBarHidden: Bool, isBackButtonHidden: Bool, title: String?) {
        
        navigationController?.navigationBar.isHidden = isBarHidden
        configureBar()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setTitle(title: title)
        navigationItem.hidesBackButton = isBackButtonHidden
        
//        if let extraLeftBar = options.extraLeftBtnImage {
//
//            let extraBtn = UIButton.init(type: .custom)
//            extraBtn.setBackgroundImage(extraLeftBar, for: .normal)
//            extraBtn.sizeToFit()
//            extraBtn.addTarget(self, action: #selector(extraLeftBarItemTapped(_:)), for: .touchUpInside)
//            navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: extraBtn)]
//        }
//
//        if navigationItem.leftBarButtonItems == nil {
//
//            navigationItem.leftBarButtonItems = [UIBarButtonItem]()
//        }
//
//        if let leftBar = options.leftBtnImage {
//
//            let leftBtn = UIButton.init(type: .custom)
//            leftBtn.setBackgroundImage(leftBar, for: .normal)
//            leftBtn.sizeToFit()
//            leftBtn.addTarget(self, action: #selector(leftBarItemTapped(_:)), for: .touchUpInside)
//            navigationItem.leftBarButtonItems?.insert(UIBarButtonItem.init(customView: leftBtn), at: 0)
//
//            if options.leftBtnImage == #imageLiteral(resourceName: "Menu") {
//
//
//            }
//        }
//
//        if navigationItem.rightBarButtonItems == nil {
//
//            navigationItem.rightBarButtonItems = []
//        }
//
//        if let _ = options.rightBtnImage {
//
//            let rightItem = UIButton.init(type: .custom)
//            rightItem.setBackgroundImage(options.rightBtnImage, for: .normal)
//            rightItem.sizeToFit()
//            rightItem.addTarget(self, action: #selector(rightBarItemTapped(_:)), for: .touchUpInside)
//            let barItem = UIBarButtonItem.init(customView: rightItem)
//            navigationItem.rightBarButtonItems = [barItem]
//        }
//
//        if let _ = options.extraRightBtnImage {
//
//            let spacing = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: self, action: nil)
//            spacing.width = 20
//            navigationItem.rightBarButtonItems?.append(spacing)
//
//            let rightItem = UIButton.init(type: .custom)
//            rightItem.setBackgroundImage(options.extraRightBtnImage, for: .normal)
//            rightItem.sizeToFit()
//            rightItem.addTarget(self, action: #selector(extraRightBarItemTapped(_:)), for: .touchUpInside)
//            let barItem = UIBarButtonItem.init(customView: rightItem)
//            navigationItem.rightBarButtonItems?.append(barItem)
//        }
    }
    
    func configureBar() {
        
        if navigationController?.isNavigationBarHidden == true {
            
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = color.themePink
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func setTitle(title: String?) {
        
        self.title = title
        _ = navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: font.title.themeRegular, NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    //MARK: - Default Bar Button Item Actions
    @objc func leftBarItemTapped(_ sender: Any) {
        
        
    }
    
    @objc func extraLeftBarItemTapped(_ sender: Any) {
        
    }
    
    @objc func rightBarItemTapped(_ sender: Any) {
        
    }
    
    @objc func extraRightBarItemTapped(_ sender: Any) {
        
    }
}

extension String {
    
    var trimmed: String {
        
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    static var signUpAttributedString: NSAttributedString {
        
        let text = NSAttributedString(string: "Don't have an account? Sign Up!", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.white
            ])
        var modifiedString = NSMutableAttributedString.init(attributedString: text)
        modifiedString = modifiedString.addColorAttribute(onText: "Sign Up!", color: color.themePinkText)
        return modifiedString
    }
    
    static var signInAttributedString: NSAttributedString {
        
        let text = NSAttributedString(string: "Already have an account? Sign In!", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.white
            ])
        var modifiedString = NSMutableAttributedString.init(attributedString: text)
        modifiedString = modifiedString.addColorAttribute(onText: "Sign In!", color: color.themePinkText)
        return modifiedString
    }
    
    func isValidEmail() -> Bool {
        
        let stricterFilterRegex = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterRegex)
        return emailTest.evaluate(with: self)
    }
}

extension NSAttributedString {
    
    func addColorAttribute(onText: String?, color: UIColor) -> NSMutableAttributedString {
        
        let string = self.string
        let modifiedString = NSMutableAttributedString.init(attributedString: self)
        var start = string.startIndex
        
        if let text = onText {
            
            while let range = string.range(of: text, options: [.literal, .caseInsensitive], range: start..<string.endIndex) {
                
                modifiedString.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: NSRange(range, in: string))
                
                start = range.upperBound
            }
        }
        
        return modifiedString
    }
}
