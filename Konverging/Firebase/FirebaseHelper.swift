import Foundation
import Firebase

struct firebaseHelper {
    
    static let shared = firebaseHelper()
    let userDetailsRef = "userDetails"
    let connectionRef = "Connection"
    
    init() {
        
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
    }
    
    static func currentUser() -> User{
        
       return Auth.auth().currentUser!
    }
    
    func login(withEmail email: String?, password: String?, didLogin: @escaping (User) -> Void, didFail: ((String) -> Void)? = nil) {
        
        Auth.auth().signIn(withEmail: email ?? "", password: password ?? "") { user, error in
            
            if let user = user {
                
                didLogin(user)
            }
            
            else {
                
                didFail?(error?.localizedDescription ?? "")
            }
        }
    }
    
    func createNewUser(email: String?, password: String?, name: String?, country: String?, countryCode: String?, companyName: String?, type: Int?, didRegister: @escaping (User) -> Void, didFail: ((String) -> Void)? = nil) {
        
        let emailID = email ?? ""
        let enteredPassword = password ?? ""
        Auth.auth().createUser(withEmail: emailID, password: enteredPassword) { user, error in
            
            if let user = user {
                
                self.addUserDetails(OfUser: user, name: name, country: country, countryCode: countryCode, companyName: companyName, type: type)
                self.login(withEmail: emailID, password: enteredPassword, didLogin: { (user) in
                    
                    didRegister(user)
                    
                }, didFail: { (loginError) in
                    
                    didFail?(loginError)
                })
            }
            
            else {
                
                didFail?(error?.localizedDescription ?? "")
            }
        }
    }
    
    private func addUserDetails(OfUser user: User, name: String?, country: String?, countryCode: String?, companyName: String?, type: Int?) {
        
        let userDetails = Database.database().reference(withPath: userDetailsRef)
        var details = [String: Any]()
        details["name"] = name
        details["email"] = firebaseHelper.currentUser().email
        details["id"] = firebaseHelper.currentUser().uid
        details["country"] = country
        details["isoCountryCode"] = countryCode
        details["companyName"] = companyName
        details["type"] = type
        userDetails.child(user.uid).setValue(details)
    }
    
    func sendForgotPasswordEmail(email: String?, didSucceed: @escaping () -> Void, didFail: ((String) -> Void)? = nil) {
        
        let actionCodeSettings =  ActionCodeSettings.init()
        actionCodeSettings.handleCodeInApp = false
        actionCodeSettings.setIOSBundleID("com.developer.Konverging")
        Auth.auth().sendPasswordReset(withEmail: email ?? "", actionCodeSettings: actionCodeSettings) { (error) in
            
            if let error = error {
                
                didFail?(error.localizedDescription)
            }
            
            else {
                
                didSucceed()
            }
        }
    }
    
    func getUserData(){
        
        let userDetails = Database.database().reference(withPath: userDetailsRef).child(firebaseHelper.currentUser().uid)
        userDetails.observeSingleEvent(of: .value) { (data) in
            
            ApplicationData.sharedInstance.saveLoginData(data: data.value as! [String:Any])
        }
    }
    
    func updateUserDetails(dict:[String:Any]) {
        
        let userDetails = Database.database().reference(withPath: userDetailsRef)
        var details = [String: Any]()
        details["name"] = dict["name"] as? String
        details["country"] = dict["country"] as? String
        details["isoCountryCode"] = dict["isoCountryCode"] as? String
        details["companyName"] = dict["companyName"] as? String
        details["dob"] = dict["dob"] as? String
        details["city"] = dict["city"] as? String
        details["PhoneNo"] = dict["PhoneNo"] as? String
      
        userDetails.child(firebaseHelper.currentUser().uid).updateChildValues(details)
    }
    
    func sendInvitation(toId: String,fromId: String,toEmailId: String,status: String) {
        
        let userDetails = Database.database().reference(withPath: connectionRef)
        var details = [String: Any]()
        details["toId"] = toId
        details["fromId"] = fromId
        details["toEmailId"] = toEmailId
        details["status"] = status
        
        userDetails.childByAutoId().setValue(details)
    }
    
    func geAllUserEmails(success:@escaping (([[String:Any]]) -> Void)){
        
        let userDetails = Database.database().reference(withPath: userDetailsRef)
        userDetails.observeSingleEvent(of: .value) { (data) in
            
            var arrDict = [[String:Any]]()
            for child in data.children {
                let snap = child as! DataSnapshot
                if let dict = snap.value as? [String:Any]{
                    
                    arrDict.append(dict)
                }
            }
            
            success(arrDict)
            
        }
        
    }
    
    
    func geAllConnection(success:@escaping (([[String:Any]]) -> Void)){
        
        let userDetails = Database.database().reference(withPath: connectionRef)
        userDetails.observeSingleEvent(of: .value) { (data) in
            
            var arrDict = [[String:Any]]()
            for child in data.children {
                let snap = child as! DataSnapshot
                if let dict = snap.value as? [String:Any]{
                    
                    arrDict.append(dict)
                }
            }
            
            success(arrDict)
            
        }
        
    }
}
