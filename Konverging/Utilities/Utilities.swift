import UIKit
import MobileCoreServices
import AVKit

struct PickerMediaType {
    
    static let image = kUTTypeImage as String
    static let video = kUTTypeMovie as String
}

class Utilities: NSObject {
  
    //MARK: Decorate view with border and corner radius
    class func decorateView(_ layer: CALayer, cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor){
        
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
    
    //MARK: Shadow Effect View
  
    class func applyShadowEffect(view: UIView){
        
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.7
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 2.0
    }
    
    class func applyShadow(view: UIView){
        
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.6
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        //view.layer.shadowRadius = 2.0
    }
    
    //MARK: Get Celll Data
    
    class func getModel(placeholder : String?,userText: String? ,imageName:String?,keyboardType:UIKeyboardType?,type : CellType) -> CellModel {
        
        let model = CellModel()
        model.userText = userText
        model.placeholder = placeholder
        model.imageName = imageName
        model.cellType = type
        model.keyboardType = keyboardType
        return model
    }
    
    
    //MARK: Text Height
    
    class func getLabelHeight(constraintedWidth width: CGFloat, font: UIFont,text:String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    class func getLabelWidth(constraintedheight: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: constraintedheight))
        label.numberOfLines = 1
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.width
    }
    
    class func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    class func loadImage(_ imageName: String) -> UIImage {
        
        return UIImage(contentsOfFile: imageName) ?? #imageLiteral(resourceName: "image_Chat")
    }
        
    //MARK: Get UIColor from hex color
  
    static func colorWithHexString (hex:String) -> UIColor {
      var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
      
      if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
      }
      
      if ((cString.count) != 6) {
        return UIColor.gray
      }
      
      var rgbValue:UInt32 = 0
      Scanner(string: cString).scanHexInt32(&rgbValue)
      
      return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
      )
    }
  
    //MARK:- Show Alert View
    
    class func showAlertView(title: String?, message: String?) {
      
        let alert = UIAlertController(title: StringConstants.AppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.kOk, style: .default, handler: nil))
      
        UIViewController.current().present(alert, animated: true, completion: nil)
    }
    
    class func showAlertWithButtonAction(title: String,message:String,buttonTitle:String,onOKClick: @escaping () -> ()){
      
        let alert = UIAlertController(title: StringConstants.AppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
            onOKClick()
        }))
      
        UIViewController.current().present(alert, animated: true, completion: nil)
    }
    
    class func showAlertWithTwoButtonAction(title: String,message:String,buttonTitle1:String,buttonTitle2:String,onButton1Click: @escaping () -> (),onButton2Click: @escaping () -> ()){
      
        let alert = UIAlertController(title: StringConstants.AppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: buttonTitle1, style: .default, handler: { action in
            onButton1Click()
        }))
        
        alert.addAction(UIAlertAction(title: buttonTitle2, style: .default, handler: { action in
            onButton2Click()
        }))
      
        UIViewController.current().present(alert, animated: true, completion: nil)
    }
    
     //MARK: Call
    class func call(phoneNumber: String)
    {
        let trimPhone = phoneNumber.replacingOccurrences(of: " ", with: "")
        if  let url = URL(string: "tel://\(trimPhone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK: Open mail
    class func openEmail(email: String)
    {
        if let url = URL(string: "mailto:\(email)")  {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK: Open URL in Safari
    
    class func openUrl(strUrl:String)
    {
        let url = URL(string: strUrl)
        if UIApplication.shared.canOpenURL(url!)
        {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    //MARK: Device Unique ID
    
    class func getDeviceUniqueID() -> String{
        
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    
    }
    
    //MARK: Open Date Picker
    class func openDatePicker(datePicker:GMDatePicker, delegate:GMDatePickerDelegate)
    {
        datePicker.delegate = delegate
        
        datePicker.config.startDate = Date()
        
        datePicker.config.animationDuration = 0.5
        
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Done"
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = color.themePink
        
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
//        datePicker.config.confirmButtonFont = FontScheme.kFontMedium(size: 16)
//        datePicker.config.cancelButtonFont = FontScheme.kFontMedium(size: 16)
        
        datePicker.show(inVC: UIViewController.current())
    }
    
    //MARK: Imagepicker controller
    class func open_galley_or_camera(delegate : UIImagePickerControllerDelegate,mediaType:[String]){
        let actionSheetController: UIAlertController = UIAlertController(title: "Choose option", message: nil, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { action -> Void in
            if(  UIImagePickerController.isSourceTypeAvailable(.camera))
                
            {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
                myPickerController.sourceType = .camera
                myPickerController.mediaTypes = mediaType
                UIViewController.current().present(myPickerController, animated: true, completion: nil)
            }
            else
            {
                let actionController: UIAlertController = UIAlertController(title: "Camera is not available",message: "", preferredStyle: .alert)
                let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void     in
                    //Just dismiss the action sheet
                }
                
                actionController.addAction(cancelAction)
                UIViewController.current().present(actionController, animated: true, completion: nil)
                
            }
        }
        actionSheetController.addAction(takePictureAction)
        
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Gallery", style: .default) { action -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate);
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = mediaType
            
            UIViewController.current().present(myPickerController, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        UIViewController.current().present(actionSheetController, animated: true, completion: nil)
        
    }
  
    //MARK: Videopicker controller
    class func openVideoPicker(delegate : UIImagePickerControllerDelegate){
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Choose option", message: nil, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { action -> Void in
            if(  UIImagePickerController.isSourceTypeAvailable(.camera))
                
            {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
                myPickerController.sourceType = .camera
                myPickerController.mediaTypes = [kUTTypeMovie as String]
                UIViewController.current().present(myPickerController, animated: true, completion: nil)
            }
            else
            {
                let actionController: UIAlertController = UIAlertController(title: "Camera is not available",message: "", preferredStyle: .alert)
                let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void     in
                    //Just dismiss the action sheet
                }
                
                actionController.addAction(cancelAction)
                UIViewController.current().present(actionController, animated: true, completion: nil)
                
            }
        }
        actionSheetController.addAction(takePictureAction)
        
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Gallery", style: .default) { action -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate);
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String]
            
            UIViewController.current().present(myPickerController, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        UIViewController.current().present(actionSheetController, animated: true, completion: nil)
        
    }
    
}

