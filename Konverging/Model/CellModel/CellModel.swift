import UIKit

class CellModel: NSObject {
    
    var placeholder : String?
    var userText : String?
    var userText2 : String?
    var cellType : CellType?
    var cellObj : Any?
    var imageName: String?
    var keyboardType: UIKeyboardType?
    var isSelected : Bool = false
    var dataArr = [Any]()
    
    var from : String?
    var to : String?
    
    var selectedIndex : Int?
    
    override init() {
    }
    
}
