import UIKit

class SectionModel: NSObject {

    var sectionTitle : String?
    var sectionDescription : String?
    var cellData = [CellModel]()
    var sectionHeight : CGFloat?
    var sectionDataCount : Int = 0
    var sectionType : SectionType?
    var isSelected = false
    
    override init() {
        sectionDataCount = cellData.count
    }
}

enum SectionType {
    case PersonalInfoSection
    case BioSection
    
    case SectionHeaderCell
}
