import UIKit

class ConnectionModel: NSObject {

    public var fromId : String?
    public var toId : String?
    public var toEmailId : String?
    public var status : String?
    
    required init(dict:[String:Any]) {
        
        fromId = dict["fromId"] as? String
        toId = dict["toId"] as? String
        toEmailId = dict["toEmailId"] as? String
        status = dict["status"] as? String
        
    }
    
}
