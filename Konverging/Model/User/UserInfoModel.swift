import AlamofireObjectMapper
import ObjectMapper

class UserInfoModel: NSObject, Mappable, NSCoding {
    
    public var companyName : String?
    public var country : String?
    public var isoCountryCode : String?
    public var name : String?
    public var type : Int?
    public var dob : String?
    public var city : String?
    public var PhoneNo : String?
    
    override init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        companyName                 <- map["companyName"]
        country                   <- map["country"]
        isoCountryCode                  <- map["isoCountryCode"]
        name                   <- map["name"]
        type                <- map["type"]
       
        dob                  <- map["dob"]
        city                   <- map["city"]
        PhoneNo                <- map["PhoneNo"]

    }
    
    // Encode decode
    required init?(coder aDecoder: NSCoder) {
        companyName               = aDecoder.decodeObject(forKey: "companyName") as? String
        name                 = aDecoder.decodeObject(forKey: "name") as? String
        country                = aDecoder.decodeObject(forKey: "country") as? String
        isoCountryCode                 = aDecoder.decodeObject(forKey: "isoCountryCode") as? String
        type              = aDecoder.decodeObject(forKey: "type") as? Int
        
        dob                  = aDecoder.decodeObject(forKey: "dob") as? String
        city                 = aDecoder.decodeObject(forKey: "city") as? String
        PhoneNo              = aDecoder.decodeObject(forKey: "PhoneNo") as? String
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(companyName, forKey: "companyName")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(country, forKey: "country")
        aCoder.encode(isoCountryCode, forKey: "isoCountryCode")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(dob, forKey: "dob")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(PhoneNo, forKey: "PhoneNo")
       
    }
}
