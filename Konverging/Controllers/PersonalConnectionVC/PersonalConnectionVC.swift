import UIKit

class PersonalConnectionVC: UIViewController {

    //MARK: Variables
    
    //MARK: Outlest
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inititalConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Initial config
    func inititalConfig(){
        tableView.register(UINib(nibName: "ConnectionCell", bundle: nil), forCellReuseIdentifier: "ConnectionCell")
    }
}

//MARK: UITableView's DataSource & Delegates Methods
extension PersonalConnectionVC: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCell") as! ConnectionCell
        
        return cell
    }
    
}
