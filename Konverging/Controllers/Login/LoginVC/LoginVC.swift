import UIKit

class LoginVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    //MARK: - Variables
    var loggedInEmail: String? = ""
    fileprivate var returnKeyHandler: keyboardReturnKeyHandler?
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialSetUp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
        returnKeyHandler = nil
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let welcomeVC = segue.destination as? ViewController {
            
            welcomeVC.loggedInEmail = loggedInEmail
        }
    }
    
    //MARK: - IBActions
    @IBAction func didTapLoginButton(_ sender: Any) {
        
//        let homevc = HomeVC.init(nibName: "HomeVC", bundle: nil)
//        self.navigationController?.pushViewController(homevc, animated: true)
        
        login()
    }
    
    @IBAction func didTapForgotPasswordButton(_ sender: Any) {
        
        showForgotPasswordAlert()
    }
    
    @IBAction func didTapRegisterButton(_ sender: Any) {
        
        performSegue(withIdentifier: "PreRegisterVCSegue", sender: self)
    }
    
    fileprivate func initialSetUp() {
        
        navigationController?.navigationBar.isHidden = true
        setUpBackgroundTap()
        registerButton.setAttributedTitle(String.signUpAttributedString, for: .normal)
        returnKeyHandler = initializeReturnKeyHandler()
    }
    
    fileprivate func login() {
        
        let email = emailTextField.text?.trimmed
        let password = passwordTextField.text
        
        let (result, message) = validate(email: email, password: password)
        
        if result {
            
            loader.show()
            firebaseHelper.shared.login(withEmail: email, password: password, didLogin: { (user) in
                
                self.loggedInEmail = user.email
                var data = [String:Any]()
                data["emailAddress"] = user.email
                ApplicationData.sharedInstance.saveLoginData(data: data)
                //self.performSegue(withIdentifier: "WelcomeVCSegue", sender: self)
                let homevc = HomeVC.init(nibName: "HomeVC", bundle: nil)
                self.navigationController?.pushViewController(homevc, animated: true)
                loader.dismiss()
                
            }) { (error) in
                
                loader.dismiss()
                self.showAlert(message: error)
            }
        }
            
        else {
            
            showAlert(message: message)
        }
    }
    
    fileprivate func validate(email: String?, password: String?) -> (Bool, String) {
        
        var result = true
        var message = ""
        
        if email?.isValidEmail() != true {
            
            result = false
            message = "Please enter a valid email address."
        }
            
        else if password?.isEmpty != false {
            
            result = false
            message = "Please enter a password."
        }
        
        return (result, message)
    }
    
    
    
    fileprivate func forgotPassword(email: String?) {
        
        loader.show()
        firebaseHelper.shared.sendForgotPasswordEmail(email: email, didSucceed: {
            
            loader.dismiss()
            self.showAlert(message: "Please check your registered email to reset your password.")
            
        }) { (error) in
            
            loader.dismiss()
            self.showAlert(message: error)
        }
    }
}

extension LoginVC {
    
    func showForgotPasswordAlert() {
        
        let alert = UIAlertController.init(title: "Password Reset", message: "Please, enter the email you have registered with.", preferredStyle: .alert)
        
        let resetAction = UIAlertAction.init(title: "Reset", style: .default) { (action) in
            
            if let emailTextField = alert.textFields?.first {
                
                self.forgotPassword(email: emailTextField.text)
            }
        }
        
        resetAction.isEnabled = false
        alert.addAction(resetAction)
        
        alert.addTextField { (textfield) in
            
            textfield.placeholder = "Registered Email"
            textfield.keyboardType = .emailAddress
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textfield, queue: OperationQueue.main, using: { (notification) in
                
                resetAction.isEnabled = textfield.text?.isValidEmail() ?? false
            })
        }
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
