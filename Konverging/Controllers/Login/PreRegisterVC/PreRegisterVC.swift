import UIKit
enum accountType: Int {
    
    case personal = 1
    case corporate = 2
}

class PreRegisterVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var accountTypeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    //MARK: - Variables
    fileprivate var selectedAccountType: accountType?
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loginButton.setAttributedTitle(String.signInAttributedString, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - IBActions
    @IBAction func didTapBackButton(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapAccountTypeButton(_ sender: Any) {
        
        selectAccountType()
    }
    
    @IBAction func didTapNextButton(_ sender: Any) {
        
        moveToNextStep()
    }
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    fileprivate func setAccountType(type: accountType) {
        
        selectedAccountType = type
        let accountType = type == .personal ? "Personal" : "Corporate"
        accountTypeButton.setTitle(accountType, for: .normal)
    }
    
    fileprivate func moveToNextStep() {
        
        if selectedAccountType == nil {
            
            showAlert(message: "Please Select the Account Type.")
            return
        }
        
        var segue = "RegisterCompanyVCSegue"
        if selectedAccountType == .personal {
            
            segue = "RegisterUserVCSegue"
        }
        performSegue(withIdentifier: segue, sender: self)
    }
}

extension PreRegisterVC {
    
    func selectAccountType() {
        
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Personal", style: .default, handler: { (action) in
        
            self.setAccountType(type: .personal)
        }))
        alert.addAction(UIAlertAction.init(title: "Corporate", style: .default, handler: { (action) in
            
            self.setAccountType(type: .corporate)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .destructive, handler: nil))
        alert.view.tintColor = .black
        present(alert, animated: true, completion: nil)
    }
}
