import UIKit

class RegisterUserVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    //MARK: - Variables
    fileprivate var selectedCountryCode = ""
    fileprivate var loggedInEmail: String? = ""
    fileprivate var returnKeyHandler: keyboardReturnKeyHandler?
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialSetUp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
        returnKeyHandler = nil
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let welcomeVC = segue.destination as? ViewController {
            
            welcomeVC.loggedInEmail = loggedInEmail
        }
    }
    
    //MARK: - IBActions
    @IBAction func didTapBackButton(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapCountryButton(_ sender: Any) {
        
        selectCountry()
    }
    
    @IBAction func didTapRegisterButton(_ sender: Any) {
        
        register()
    }
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        
        navigationController?.popToRootViewController(animated: true)
    }
    
    fileprivate func initialSetUp() {
        
        setUpBackgroundTap()
        loginButton.setAttributedTitle(String.signInAttributedString, for: .normal)
        returnKeyHandler = initializeReturnKeyHandler()
    }
    
    fileprivate func selectCountry() {
        
        showCountryPicker(didSelectCountry: { (country, code) in
            
            self.countryButton.setTitle(country, for: .normal)
            self.selectedCountryCode = code
        })
    }
    
    
    fileprivate func register() {
        
        let name = nameTextField.text?.trimmed
        let companyName = companyNameTextField.text?.trimmed
        let email = emailTextField.text?.trimmed
        let password = passwordTextField.text
        let country = countryButton.currentTitle
        
        let (result, message) = validate(name: name, email: email, password: password, companyName: companyName)
        
        if result {
            
            loader.show()
            firebaseHelper.shared.createNewUser(email: email, password: password, name: name, country: country, countryCode: selectedCountryCode, companyName: companyName, type: 2, didRegister: { (user) in
                
                self.loggedInEmail = user.email
                //self.performSegue(withIdentifier: "WelcomeVCSegue", sender: self)
                
                var data = [String:Any]()
                data["emailAddress"] = user.email
                ApplicationData.sharedInstance.saveLoginData(data: data)
                
                let homevc = HomeVC.init(nibName: "HomeVC", bundle: nil)
                self.navigationController?.pushViewController(homevc, animated: true)
                
                loader.dismiss()
                
            }, didFail: { (error) in
                
                loader.dismiss()
                self.showAlert(message: error)
            })
        }
            
        else {
            
            showAlert(message: message)
        }
    }
    
    fileprivate func validate(name: String?, email: String?, password: String?, companyName: String?) -> (Bool, String) {
        
        var result = true
        var message = ""
            
        if name?.isEmpty != false {
            
            result = false
            message = "Please enter your first and last name."
        }
            
        else if selectedCountryCode.isEmpty != false {
            
            result = false
            message = "Please select a country."
        }
            
        else if companyName?.isEmpty != false {
            
            result = false
            message = "Please enter company name."
        }
            
        else if email?.isValidEmail() != true {
            
            result = false
            message = "Please enter a valid email address."
        }
            
        else if password?.isEmpty != false {
            
            result = false
            message = "Please enter a password."
        }
            
        else if password?.count ?? 0 < 6 {
            
            result = false
            message = "Password must be a minimum of 6 characters."
        }
        
        return (result, message)
    }
}
