import UIKit

class HomeVC: UIViewController {

    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       self.setUpNavigationBar(isBarHidden: false, isBackButtonHidden: true, title: "Home")
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        firebaseHelper.shared.getUserData()
        self.addBarButtons()
    }

    //MARK: Add Bar Buttons
    func addBarButtons(){
        
        let btnRefresh = UIButton.init(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnRefresh.setImage(#imageLiteral(resourceName: "Menu"), for: .normal)
        btnRefresh.addTarget(self, action: #selector(self.btnMenu_Click(_sender:)), for: .touchUpInside)
        
        let item2 = UIBarButtonItem(customView: btnRefresh)
        self.navigationItem.setRightBarButton(item2, animated: true)
    }
    
    //MARK: IBAction
    
    @IBAction func btnMenu_Click(_sender : UIButton) {
        
        let vc = SettingsVC.init(nibName: "SettingsVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogout_Click(_ sender: UIButton) {
        
        ApplicationData.sharedInstance.logoutUser()
    }
    
}
