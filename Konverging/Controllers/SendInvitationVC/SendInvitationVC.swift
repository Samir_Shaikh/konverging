import UIKit

class SendInvitationVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtEmailAddress: UITextField!
    
    //MARK: variables
    var arrCellData = [CellModel]()
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpNavigationBar(isBarHidden: false, isBackButtonHidden: false, title: "Send Invitations")
    }
    
    //MARK: Initial Config
    func initialConfig(){
        
        tableView.register(UINib(nibName: "HeaderSectionCell", bundle: nil), forCellReuseIdentifier: "HeaderSectionCell")
        tableView.register(UINib(nibName: "ConnectionCell", bundle: nil), forCellReuseIdentifier: "ConnectionCell")
        self.prepareDataSource()
    }

    func prepareDataSource(){
        arrCellData.append(Utilities.getModel(placeholder: "", userText: nil, imageName: "", keyboardType: .default, type: .InvitationHeaderCell))
        
        firebaseHelper.shared.geAllUserEmails { (arrDict) in
            
            for obj in arrDict{
                
                if obj["email"] as! String != firebaseHelper.currentUser().email!{
                    
                    self.arrCellData.append(Utilities.getModel(placeholder: obj["name"] as? String, userText: obj["companyName"] as? String, imageName: "", keyboardType: .default, type: .InvitationMainCell))
                }
            }
        
            self.tableView.reloadData()
        }
        

    }
    
    //MARK: Button action methods
    @IBAction func btnPlus_Click(_ sender: UIButton) {
        
        if txtEmailAddress.text != nil && txtEmailAddress.text!.isValidEmail(){
            
            var isEmailFound = false
            firebaseHelper.shared.geAllUserEmails(success: { (arrDict) in
                
                for (obj) in arrDict{
                    
                    if obj["email"] as! String == self.txtEmailAddress.text!{
                        
                        if obj["email"] as! String == firebaseHelper.currentUser().email!{
                            
                            Utilities.showAlertView(title: StringConstants.AppName, message: "You can not send invitation to your email.")
                            return
                        }
                        
                        isEmailFound = true
                        firebaseHelper.shared.sendInvitation(toId: obj["id"] as! String, fromId: firebaseHelper.currentUser().uid, toEmailId: self.txtEmailAddress.text!, status: StringConstants.InvitationStatus.Pending)
                        Utilities.showAlertView(title: StringConstants.AppName, message: "Invitation send succesfully.")
                    }
                }
                
                if !isEmailFound{
                    Utilities.showAlertView(title: StringConstants.AppName, message: "Email not found.")
                }
                
            })
           
        
        }else{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please enter valid email.")
        }
    }
}


//MARK: UITableView's DataSource & Delegates Methods
extension SendInvitationVC: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData[indexPath.row]
        
        if model.cellType == CellType.InvitationHeaderCell{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderSectionCell") as! HeaderSectionCell
            cell.lblTitle.text = "Recommandation"
            cell.lblDesc.text = "Your contacts that alreay have an account with us"
            cell.imgArrow.isHidden = true
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCell") as! ConnectionCell
            cell.imgCancelSign.image = #imageLiteral(resourceName: "add icon@2x")
            cell.lblName.text = model.placeholder
            cell.lblCompanyName.text = model.userText
            return cell
        }
       
    }
    
}
