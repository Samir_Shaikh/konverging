import UIKit

class ActivityProfilePagerVC: ViewPagerController {

    //MARK: variables
    var arrTabs = [PagerTabs.Activity, PagerTabs.Profile, PagerTabs.Network]
    var tabSelected =  PagerType.Activity
    var tabIndex = Int()
    
    //MARK: Outlets
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblNetwork: UILabel!
    
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Initial Config
    func initialConfig(){
        self.tabHeight = 0
        self.delegate = self
        self.dataSource = self
        DispatchQueue.main.async {
            self.checkTabSelection()
            self.selectTab(at: UInt(self.tabIndex))
        }
        self.tabsViewBackgroundColor = UIColor.lightGray
    }
    
    //MARK: Private methods
    func checkTabSelection(){
        
        switch tabSelected {
        case .Activity:
            
            lblNetwork.textColor = UIColor.black
            lblActivity.textColor = color.themePink
            lblProfile.textColor = UIColor.black
            self.title = "\(ApplicationData.user.name ?? "")'s Activity"
            break
            
        case .Profile:
            
            lblNetwork.textColor = UIColor.black
            lblActivity.textColor = UIColor.black
            lblProfile.textColor = color.themePink
            self.title = "\(ApplicationData.user.name ?? "")'s Profile"
            break
            
        case .Network:
            
            lblNetwork.textColor = color.themePink
            lblActivity.textColor = UIColor.black
            lblProfile.textColor = UIColor.black
            self.title = "\(ApplicationData.user.name ?? "")'s Network"
            break
        }
    }
    
    //MARK: Button action methods
    @IBAction func btnActivity_Click(_ sender: UIButton) {
        
        tabSelected = .Activity
        self.selectTab(at: 0)
        self.checkTabSelection()
    }
    
    @IBAction func btnProfile_Click(_ sender: UIButton) {
        
        tabSelected = .Profile
        self.selectTab(at: 1)
        self.checkTabSelection()
    }
    
    @IBAction func btnNetwork_Click(_ sender: UIButton) {
        
        tabSelected = .Network
        self.selectTab(at: 2)
        self.checkTabSelection()
    }
    
}


//MARK: View pager delegate methods
extension ActivityProfilePagerVC: ViewPagerDelegate, ViewPagerDataSource{
    
    func numberOfTabs(forViewPager viewPager: ViewPagerController!) -> UInt {
        return UInt(arrTabs.count)
    }
    
    
    func viewPager(_ viewPager: ViewPagerController!, viewForTabAt index: UInt) -> UIView! {
        
        return UIView()
    }
    
    func viewPager(_ viewPager: ViewPagerController!, contentViewControllerForTabAt index: UInt) -> UIViewController! {
        let tabs = arrTabs[Int(index)]
        
        if tabs == PagerTabs.Activity{
            
        }else  if tabs == PagerTabs.Profile{
            
            let profileVC = ProfileVC(nibName: "ProfileVC", bundle: nil)
            return profileVC
        }else{
            
            let netWork = NetworkVC(nibName: "NetworkVC", bundle: nil)
            return netWork
        }
        return UIViewController()
    }
    
    func viewPager(_ viewPager: ViewPagerController!, didChangeTabTo index: UInt) {
        if Int(index) == 0{
            tabSelected = .Activity
            self.checkTabSelection()
        }else if Int(index) == 1{
            tabSelected = .Profile
            self.checkTabSelection()
        }else if Int(index) == 2{
            tabSelected = .Network
            self.checkTabSelection()
        }
    }
}
