import UIKit

class SettingsVC: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var tblSettings: UITableView!
    
    //MARK: Variables
    var arrCellData = [CellModel]()
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpNavigationBar(isBarHidden: false, isBackButtonHidden: false, title: "Settings")
    }
    
    //MARK: Private Methods
    
    func initialConfig(){
        
        tblSettings.register(UINib(nibName: "SettingsHeaderCell", bundle: nil), forCellReuseIdentifier: "SettingsHeaderCell")
        
        prepareDataSource()
    }
    
    func prepareDataSource(){
        
        arrCellData.append(Utilities.getModel(placeholder: "", userText: nil, imageName: "", keyboardType: .default, type: CellType.SettingsHeader))
        
    }
}

//MARK: UITableView's DataSource & Delegates Methods
extension SettingsVC: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsHeaderCell") as! SettingsHeaderCell
        
        return cell
    }
    
}

