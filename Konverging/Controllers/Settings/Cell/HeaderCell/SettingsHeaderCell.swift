import UIKit
import MobileCoreServices
class SettingsHeaderCell: UITableViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        
        imgUser.layer.cornerRadius = imgUser.bounds.size.width / 2
        imgUser.clipsToBounds = true
        
        lblName.text = ApplicationData.user.name
        lblCompany.text = ApplicationData.user.companyName
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnNetwork_Click(_ sender: UIButton) {
        let pager = ActivityProfilePagerVC(nibName: "ActivityProfilePagerVC", bundle: nil)
        pager.tabSelected = .Network
        pager.tabIndex = 2
        UIViewController.current().navigationController?.pushViewController(pager, animated: true)
    }
    
    @IBAction func btnActivity_Click(_ sender: UIButton) {
        let pager = ActivityProfilePagerVC(nibName: "ActivityProfilePagerVC", bundle: nil)
        pager.tabSelected = .Activity
        pager.tabIndex = 0
        UIViewController.current().navigationController?.pushViewController(pager, animated: true)
    }
    
    @IBAction func btnProfile_Click(_ sender: UIButton) {
        let pager = ActivityProfilePagerVC(nibName: "ActivityProfilePagerVC", bundle: nil)
        pager.tabSelected = .Profile
        pager.tabIndex = 1
        UIViewController.current().navigationController?.pushViewController(pager, animated: true)
    }
    
    @IBAction func btnChangeProfilePic_Click(_ sender: UIButton) {
        Utilities.open_galley_or_camera(delegate: self, mediaType: [kUTTypeImage as String])
    }
}

extension SettingsHeaderCell : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)

        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgUser.image = pickedImage
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
