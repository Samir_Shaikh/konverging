import UIKit

class HeaderSectionCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnHeaderSelection: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblBorder: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(model : SectionModel){
        if model.isSelected == true{
            imgArrow.isHidden = true
            lblBorder.isHidden = false
        }else{
            imgArrow.isHidden = false
            lblBorder.isHidden = true
        }
        
        lblDesc.text = model.sectionDescription
        lblTitle.text = model.sectionTitle
    }
}
