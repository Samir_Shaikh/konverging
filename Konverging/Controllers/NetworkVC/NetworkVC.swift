import UIKit

class NetworkVC: UIViewController {

    //MARK: Variables
    var arrSection = [SectionModel]()
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblConnectionCount: UILabel!
    var arrAllConnection = [ConnectionModel]()
    var arrMyConnection = [ConnectionModel]()
    var arrInvitation = [ConnectionModel]()
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inititalConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Initial Config
    func inititalConfig(){
        tableView.register(UINib(nibName: "HeaderSectionCell", bundle: nil), forCellReuseIdentifier: "HeaderSectionCell")
        tableView.register(UINib(nibName: "NetworkMainCell", bundle: nil), forCellReuseIdentifier: "NetworkMainCell")
        
        firebaseHelper.shared.geAllConnection { (arrDict) in
            
            for obj in arrDict{
                
                self.arrAllConnection.append(ConnectionModel(dict: obj))
            }
            
            self.prepareDataSource()
        }
        
        
    }

    func prepareDataSource(){
        
        for model in arrAllConnection{
            
            if model.toId == firebaseHelper.currentUser().uid && model.status == "1"{
                
                self.arrMyConnection.append(model)
            }
        }
        
        for model in arrAllConnection{
            
            if model.toId == firebaseHelper.currentUser().uid && model.status == StringConstants.InvitationStatus.Pending{
                
                self.arrInvitation.append(model)
            }
        }
        
        lblConnectionCount.text = "\(arrMyConnection.count)"
       
        firebaseHelper.shared.geAllUserEmails { (arrDict) in
            
            let sectionData = SectionModel()
            sectionData.sectionTitle = "Invitation (\(self.arrInvitation.count))"
            sectionData.sectionType = .SectionHeaderCell
            sectionData.isSelected = true
            
            
            for obj in arrDict{
            
                for model in self.arrInvitation{
                    
                    if obj["id"] as? String == model.fromId{
                        
                        sectionData.cellData.append(Utilities.getModel(placeholder: obj["name"] as? String, userText: obj["companyName"] as? String, imageName: "", keyboardType: .default, type: .NetworkCell))
                    }
                    
                }
                
            }
            
            self.arrSection.append(sectionData)
            
            let sectionPeople = SectionModel()
            sectionPeople.sectionTitle = "People you may know"
            sectionPeople.sectionDescription = "Recommandation based on your profile and interest"
            sectionPeople.sectionType = .SectionHeaderCell
            sectionPeople.isSelected = false
            
            for obj in arrDict{
                
                if obj["email"] as! String != firebaseHelper.currentUser().email!{
                    
                    sectionPeople.cellData.append(Utilities.getModel(placeholder: obj["name"] as? String, userText: obj["companyName"] as? String, imageName: "", keyboardType: .default, type: .NetworkCell))
                }
            }
            
            
            self.arrSection.append(sectionPeople)
            
            let sectionInstitutions = SectionModel()
            sectionInstitutions.sectionTitle = "Institutions you may follow"
            sectionInstitutions.sectionDescription = "Recommandation based on your profile and interest"
            sectionInstitutions.sectionType = .SectionHeaderCell
            sectionInstitutions.isSelected = false
            
            for obj in arrDict{
                
                if obj["email"] as! String != firebaseHelper.currentUser().email!{
                    
                    sectionInstitutions.cellData.append(Utilities.getModel(placeholder: obj["name"] as? String, userText: obj["companyName"] as? String, imageName: "", keyboardType: .default, type: .NetworkCell))
                  
                }
            }
            
            self.arrSection.append(sectionInstitutions)
            
            self.tableView.reloadData()
        }
        
        
    }
    
    //MARK: Button action method
    @IBAction func btnViewConnection_Click(_ sender: UIButton) {
        let personalVC = ConnectionsPagerVC(nibName: "ConnectionsPagerVC", bundle: nil)
        personalVC.tabSelected = .Personal
        UIViewController.current().navigationController?.pushViewController(personalVC, animated: true)
    }
    
    @IBAction func btnSendInvitation_Click(_ sender: UIButton) {
        let personalVC = SendInvitationVC(nibName: "SendInvitationVC", bundle: nil)
        UIViewController.current().navigationController?.pushViewController(personalVC, animated: true)
    }
    
}

extension NetworkVC : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSection.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderSectionCell") as!HeaderSectionCell
        cell.setCellData(model: arrSection[section])
        cell.btnHeaderSelection.tag = section
        cell.btnHeaderSelection.addTarget(self, action: #selector(btnHeaderSelected), for: .touchUpInside)
        return cell
    }
    
    @IBAction func btnHeaderSelected(sender : UIButton){
        if arrSection[sender.tag].isSelected == true{
            arrSection[sender.tag].isSelected = false
        }else{
            arrSection[sender.tag].isSelected = true
        }
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = arrSection[section].sectionDescription{
            return 96
        }
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrSection[section].isSelected == false{
            return 0
        }
        return arrSection[section].cellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrSection[indexPath.section].cellData[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NetworkMainCell", for: indexPath) as! NetworkMainCell
        cell.lblName.text = model.placeholder
        cell.lblCompany.text = model.userText
        cell.lblMutual.text = ""
        return cell
    }
}
