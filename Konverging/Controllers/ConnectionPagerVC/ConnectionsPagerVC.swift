import UIKit

class ConnectionsPagerVC: ViewPagerController {
    
    //MARK: Variables
    var arrTabs = [ConnectionPagerType.Personal, ConnectionPagerType.Company]
    var tabSelected :  ConnectionPagerType?
    var arrMyconnection = [ConnectionModel]()
    
    @IBOutlet weak var lblConnectionCount: UILabel!
    
    //MARK: outlets
    @IBOutlet weak var lblPersonal: UILabel!
    @IBOutlet weak var lblCompanies: UILabel!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpNavigationBar(isBarHidden: false, isBackButtonHidden: false, title: "Manage Connections")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialConfig(){
        
        self.lblConnectionCount.text = "Connections (\(arrMyconnection.count))"
        self.tabHeight = 0
        self.delegate = self
        self.dataSource = self
        DispatchQueue.main.async {
            self.checkTabSelection()
            self.moveToVc()
        }
        self.tabsViewBackgroundColor = UIColor.lightGray
        
    }
    
    //MARK: Private methods
    
    func moveToVc(){
        switch tabSelected {
        case .Personal?:
            self.selectTab(at: 0)
            break
        case .Company?:
            self.selectTab(at: 1)
            break
        case .none:
            break
        }
    }
    func checkTabSelection(){
        
        switch tabSelected {
        case .Personal?:
            
            lblCompanies.textColor = UIColor.black
            lblPersonal.textColor = color.themePink
            break
            
        case .Company?:
            
            lblPersonal.textColor = UIColor.black
            lblCompanies.textColor = color.themePink
            break
        case .none:
            break
        }
    }
    
    //MARK: button action
    @IBAction func btnPersonal_Click(_ sender: UIButton) {
        tabSelected = .Personal
        self.selectTab(at: 0)
        self.checkTabSelection()
    }
    
    @IBAction func btnCompany_Click(_ sender: UIButton) {
        tabSelected = .Company
        self.selectTab(at: 1)
        self.checkTabSelection()
    }
}

//MARK: View pager delegate methods
extension ConnectionsPagerVC: ViewPagerDelegate, ViewPagerDataSource{
    
    func numberOfTabs(forViewPager viewPager: ViewPagerController!) -> UInt {
        return UInt(arrTabs.count)
    }
    
    
    func viewPager(_ viewPager: ViewPagerController!, viewForTabAt index: UInt) -> UIView! {
        
        return UIView()
    }
    
    func viewPager(_ viewPager: ViewPagerController!, contentViewControllerForTabAt index: UInt) -> UIViewController! {
        let tabs = arrTabs[Int(index)]
        
        if tabs == ConnectionPagerType.Personal{
            
            let profileVC = PersonalConnectionVC(nibName: "PersonalConnectionVC", bundle: nil)
            return profileVC
        }else{
            
            let netWork = ComapnyConnectionVC(nibName: "ComapnyConnectionVC", bundle: nil)
            return netWork
        }
    }
    
    func viewPager(_ viewPager: ViewPagerController!, didChangeTabTo index: UInt) {
        if Int(index) == 0{
            tabSelected = .Personal
            self.checkTabSelection()
        }else if Int(index) == 1{
            tabSelected = .Company
            self.checkTabSelection()
        }
    }
}
