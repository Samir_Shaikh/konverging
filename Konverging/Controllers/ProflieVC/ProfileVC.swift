import UIKit

class ProfileVC: UIViewController {

    //MARK: Variables
    var arrSections = [SectionModel]()
    var arrPersonalInfoData = [CellModel]()
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: initial Config
    func initialConfig(){
        
        tableView.register(UINib(nibName: "MainCell", bundle: nil), forCellReuseIdentifier: "MainCell")
        self.prepareDataSource()
    }

    func prepareDataSource(){
        
        let personalSection = SectionModel()
        personalSection.sectionTitle = "Personal Information"
        personalSection.sectionType = .PersonalInfoSection
        arrPersonalInfoData.append(Utilities.getModel(placeholder: "Name", userText: ApplicationData.user.name, imageName: "", keyboardType: .default, type: .UserNameCell))
        arrPersonalInfoData.append(Utilities.getModel(placeholder: "EmailId", userText: firebaseHelper.currentUser().email, imageName: "", keyboardType: .emailAddress, type: .EmailIdCell))
        arrPersonalInfoData.append(Utilities.getModel(placeholder: "Date of Birth", userText: ApplicationData.user.dob, imageName: "", keyboardType: .default, type: .DateOfBirthCell))
        arrPersonalInfoData.append(Utilities.getModel(placeholder: "Country", userText: ApplicationData.user.country, imageName: "", keyboardType: .default, type: .CountryCityCell))
        arrPersonalInfoData.append(Utilities.getModel(placeholder: "Code", userText: nil, imageName: "", keyboardType: .default, type: .CountryCodePhoneNumberCell))
        
        personalSection.cellData = arrPersonalInfoData
        arrSections.append(personalSection)
        
        let userBioSection = SectionModel()
        userBioSection.sectionType = .BioSection
        userBioSection.sectionTitle = "\(ApplicationData.user.name ?? "")'s Bio"
        userBioSection.cellData.append(Utilities.getModel(placeholder: "Company", userText: ApplicationData.user.companyName, imageName: "", keyboardType: .default, type: .UserBioCell))
        arrSections.append(userBioSection)
        
        self.tableView.reloadData()
    }
    
    @IBAction func btnUpdate_Click(_ sender: UIButton) {
        
        if checkValidation(){
            
            let request = self.getRequest()
            firebaseHelper.shared.updateUserDetails(dict: request)
            Utilities.showAlertView(title: StringConstants.AppName, message: "Update Successfully.")
        }
    }
    
}

extension ProfileVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrSections[indexPath.row]

        if model.sectionType == SectionType.PersonalInfoSection{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell") as! MainCell
            cell.setCellData(model: arrSections[indexPath.row].cellData)
            cell.lblTitle.text = model.sectionTitle
        
            return cell
        
        }else if model.sectionType == SectionType.BioSection{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell") as! MainCell
            cell.setCellData(model: arrSections[indexPath.row].cellData)
            cell.lblTitle.text = model.sectionTitle
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = arrSections[indexPath.row]
        if model.sectionType == SectionType.PersonalInfoSection{
            
            return CGFloat((model.cellData.count * 64 ) + 82)
        }else if model.sectionType == SectionType.BioSection{
            
            return CGFloat((model.cellData.count * 64) + 82)
        }
        return UITableViewAutomaticDimension
    }
}

//MARK: checkValidation
extension ProfileVC{
    func getRequest() -> [String:Any]{
        var request = [String:Any]()
        
        for model in arrSections{
            for model in model.cellData{
                if model.cellType == CellType.UserNameCell{
                    request["name"] = model.userText
                }
                
                if model.cellType == CellType.EmailIdCell{
                    request["emailId"] = model.userText
                }
                
                if model.cellType == CellType.DateOfBirthCell{
                    request["dob"] = model.userText
                }
                
                if model.cellType == CellType.CountryCityCell{
                    request["country"] = model.userText
                    request["city"] = model.userText2
                }
                
                if model.cellType == CellType.CountryCodePhoneNumberCell{
                    request["isoCountryCode"] = model.userText
                    request["PhoneNo"] = model.userText2
                }
                
                if model.cellType == CellType.UserBioCell{
                    request["companyName"] = model.userText
                }
            }
        }
        
        return request
    }
    
    func checkValidation() -> Bool{
        
        var request = self.getRequest()
        if request["name"] as? String == nil || request["name"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please enter name")
            return false
            
        }else if request["emailId"] as? String == nil || request["emailId"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please enter email")
            return false
            
        }else if request["dob"]  as? String == nil || request["dob"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please select date of birth")
            
            return false
            
        }else if request["country"] as? String == nil || request["country"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please select country")
            return false
            
        }else if request["city"] as? String == nil || request["city"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please enter city")
            return false
            
        }else if request["isoCountryCode"] as? String == nil || request["isoCountryCode"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please enter country code")
            return false
            
        }else if request["PhoneNo"] as? String == nil || request["PhoneNo"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please enter phone no.")
            return false
            
        }else if request["companyName"] as? String == nil || request["companyName"] as? String == ""{
            
            Utilities.showAlertView(title: StringConstants.AppName, message: "Please enter bio")
            return false
            
        }
        
        return true
    }
}
