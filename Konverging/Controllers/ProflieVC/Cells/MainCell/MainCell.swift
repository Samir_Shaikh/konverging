import UIKit

class MainCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: varaibles
    var arrCellData : [CellModel]?
    
    //MARK: cell life cycle events
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: "UITextFieldCell", bundle: nil), forCellReuseIdentifier: "UITextFieldCell")
        tableView.register(UINib(nibName: "SelectionCell", bundle: nil), forCellReuseIdentifier: "SelectionCell")
        tableView.register(UINib(nibName: "CountryCityCell", bundle: nil), forCellReuseIdentifier: "CountryCityCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: set cell data
    func setCellData(model : [CellModel]){
        arrCellData = model
        self.tableView.reloadData()
    }
}

extension MainCell : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData![indexPath.row]
        
        if model.cellType == CellType.UserNameCell || model.cellType ==  CellType.EmailIdCell || model.cellType == CellType.UserBioCell  {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UITextFieldCell", for: indexPath) as! UITextFieldCell
            cell.setCellData(model: model)
            cell.textField.text = model.userText
            if model.cellType == .EmailIdCell{
                
                cell.textField.isUserInteractionEnabled = false
            }else{
                
                cell.textField.isUserInteractionEnabled = true
            }
            return cell
            
        }else if model.cellType == CellType.DateOfBirthCell{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath) as! SelectionCell
            cell.setCellData(model: model)
            return cell
        }else if model.cellType == CellType.CountryCityCell{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCityCell", for: indexPath) as! CountryCityCell
            cell.setCellDataForCountryCity(model: model)
            return cell
        }else if model.cellType == CellType.CountryCodePhoneNumberCell{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCityCell", for: indexPath) as! CountryCityCell
            cell.setCellDataForCountryCodePhoneNumber(model: model)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}
