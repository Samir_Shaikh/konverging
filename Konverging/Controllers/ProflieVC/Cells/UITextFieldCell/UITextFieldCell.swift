import UIKit

class UITextFieldCell: UITableViewCell {

    @IBOutlet weak var textField: RPFloatingPlaceholderTextField!
    var model : CellModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.textField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    func setCellData(model : CellModel){
        self.model = model
        self.textField.placeholder = model.placeholder ?? ""
        self.textField.keyboardType = model.keyboardType!
    }
}

extension UITextFieldCell : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != "" {
            model?.userText = textField.text
        }
    }
}
