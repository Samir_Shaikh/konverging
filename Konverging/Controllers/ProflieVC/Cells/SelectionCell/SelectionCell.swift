import UIKit

class SelectionCell: UITableViewCell {

    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var textField: RPFloatingPlaceholderTextField!
    
    var datePicker = GMDatePicker()
    var model : CellModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func btnSelectDate_Click(_ sender: UIButton) {
        
        // Validation for Maximum date
        // From date is always less than To date
        //let maxDate:Date?
        
        datePicker.datePicker.maximumDate = Date()
        Utilities.openDatePicker(datePicker: datePicker, delegate: self)
    }
    
    func setCellData(model : CellModel){
        self.model = model
        self.textField.placeholder = model.placeholder ?? ""
        self.textField.keyboardType = model.keyboardType!
        
        textField.text = ApplicationData.user.dob
    }
}

//MARK: DatePicker Delegate Methods
extension SelectionCell: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date) {
        print(date)
        
            textField.text = DateUtilities.convertStringfromDate(date: date)
            model?.userText = textField.text!
        
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        // Cancel Code here
    }
}
