import UIKit

class CountryCityCell: UITableViewCell {

    @IBOutlet weak var txtFieldCountry: RPFloatingPlaceholderTextField!
    @IBOutlet weak var txtFieldCity: RPFloatingPlaceholderTextField!
    
    var model : CellModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        txtFieldCity.delegate = self
        txtFieldCountry.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    func setCellDataForCountryCity(model : CellModel){
        self.model = model
        txtFieldCity.placeholder = "City"
        txtFieldCountry.placeholder = "Country"
        
        txtFieldCountry.text = ApplicationData.user.country
        txtFieldCity.text = ApplicationData.user.city
        
        model.userText = txtFieldCountry.text
        model.userText2 = txtFieldCity.text
    }
    
    func setCellDataForCountryCodePhoneNumber(model : CellModel){
        self.model = model

        txtFieldCity.placeholder = "Phone Number"
        txtFieldCountry.placeholder = "Country Code"
        
        txtFieldCountry.text = ApplicationData.user.isoCountryCode
        txtFieldCity.text = ApplicationData.user.PhoneNo
        
        model.userText = txtFieldCountry.text
        model.userText2 = txtFieldCity.text
    }
    
    
}

extension CountryCityCell : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtFieldCity {
            if textField.text != ""{
                model?.userText2 = textField.text
            }
        }
        
        if textField == txtFieldCountry {
            if textField.text != ""{
                model?.userText = textField.text
            }
        }
    }
}
