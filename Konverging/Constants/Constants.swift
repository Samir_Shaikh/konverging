import Foundation
import UIKit

struct device {
    
    static var isiPad: Bool {
        
        return UI_USER_INTERFACE_IDIOM() == .pad
    }
}

struct PagerTabs {
    
    static let Activity     = "Activity"
    static let Profile      = "Profile"
    static let Network      = "Network"
}

struct color {
    
    static let blackTransparent = UIColor.black.withAlphaComponent(0.5)
    static let themePink = UIColor(red: 199/255, green: 65/255, blue: 174/255, alpha: 1)
    static let themePinkText = UIColor(red: 216/255, green: 0/255, blue: 177/255, alpha: 1)
}

struct font {
    
    static let system = UIFont.systemFont(ofSize: UIFont.systemFontSize)
    
    struct size {
        
        static let title: CGFloat = device.isiPad ? 21 : 17
        static let content: CGFloat = device.isiPad ? 19 : 15
    }
    
    struct title {
        
        static let themeRegular = UIFont.systemFont(ofSize: size.title)
    }
    
    struct content {
        
        static let themeRegular = UIFont.systemFont(ofSize: size.content)
    }
}
