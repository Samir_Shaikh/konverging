import Foundation
import UIKit

struct StringConstants {
    
    static let AppName                                      = "Konverging"
    static let kOk                                      = "OK"
    
    //MARK:  User Default
    struct UserDefaultKey {
        
        static let isUserLoggedIn               = "isUserLoggedIn"
        static let userInfo                     = "userInfo"
      
    }
    
    struct InvitationStatus {
        
        static let Accept               = "1"
        static let Reject               = "2"
        static let Pending               = "3"
        
    }
}
