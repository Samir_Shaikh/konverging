import UIKit

//MARK: All Cell Enum
enum CellType {
    
    //Setting
    case SettingsHeader
    
    //Profile
    case PersonalInfoCell
    case UserBioCell
    case UserNameCell
    case EmailIdCell
    case DateOfBirthCell
    case CountryCityCell
    case CountryCodePhoneNumberCell
    
    //Network
    case NetworkCell
    
    //Send Invitationcell
    case InvitationHeaderCell
    case InvitationMainCell
}

enum PagerType {
    case Activity
    case Profile
    case Network
}

enum ConnectionPagerType{
    case Personal
    case Company
}

