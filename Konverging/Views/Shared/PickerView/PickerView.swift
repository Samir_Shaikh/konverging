import UIKit

class PickerView: UIView {

    //MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    
    //MARK: - Variables
    fileprivate var pickerData = [String]()
    var doneCallback: ((Int) -> Void)?
    var backgroundTapCallback: (() -> Void)?
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        UIApplication.shared.keyWindow?.endEditing(true)
        backgroundColor = color.blackTransparent
        containerView.roundCorners()
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK: - IBActions
    @IBAction func didTapDoneButton(_ sender: Any) {
        
        doneCallback?(picker.selectedRow(inComponent: 0))
    }
    
    @IBAction func didTapBackground(_ sender: Any) {
        
        backgroundTapCallback?()
    }
    
    //MARK: - Set Data
    func setData(title: String, data: [String]) {
        
        titleLabel.text = title
        pickerData = data
        picker.dataSource = self
        picker.delegate = self
    }
    
}

extension PickerView: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerData.count
    }
}

extension PickerView: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerData[row]
    }
}
